Lab3 

#include <stdio.h>
#include <windows.h>

void main() {
	char buffer[100];
	DWORD actlen, error;
	HANDLE hstdout, fhandle;
	char fname[]="file";
	BOOL rc;
	
	COORD coord;
	coord.X=50;
	coord.Y=13;
	
	hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
	
	SetConsoleCursorPosition(hstdout, coord);
	SetConsoleTextAttribute(hstdout, FOREGROUND_RED | FOREGROUND_BLUE);
	
	fhandle = CreateFile(fname, GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	if(fhandle == INVALID_HANDLE_VALUE)
	{
		error = GetLastError();
		if(error == 32)
		{
			printf("Error! ");
			while(fhandle == INVALID_HANDLE_VALUE)
			{
		fhandle = CreateFile(fname, GENERIC_READ, 0, 0, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, 0);
			}
		}
	}
	rc = ReadFile(fhandle, buffer, 80, &actlen, NULL);
	WriteFile(hstdout, buffer, actlen, &actlen, NULL);
	sleep(3);
	CloseHandle(fhandle);
}
